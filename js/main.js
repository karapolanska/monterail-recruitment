var app = angular.module('App1', []);
app.controller('QuestionController', function () {
    this.items = it;
});
var it = [
    {
        name: 'Eva',
        question: 'Will insulin make my patient gain weight?',
        description: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
        published: new Date('20.04.2017'),
        rate: 19,
        rated: 0,
        images: [
            {
                full: 'img/lena.jpg',
                thumb: 'img/lena.jpg'
            }
        ],
        comments: 5,
        commentsItems: [
            {
                name: 'Particia',
                published: 'yesterday',
                description: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.',
                rate: 2,
                rated: 0,
                images: [
                    {
                        full: 'img/lena.jpg',
                        thumb: 'img/lena.jpg'
                    }
                ],
                subcomments: [
                    {
                        name: 'Anna',
                        published: 'yesterday',
                        description: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.',
                        rate: 3,
                        rated: 0,
                        images: [
                            {
                                full: 'img/lena.jpg',
                                thumb: 'img/lena.jpg'
                            }
                        ]
                    }
                ]
            },
            {
                name: 'Julia',
                published: '20.04.2017',
                description: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.',
                rate: 12,
                rated: 0,
                images: [
                    {
                        full: 'img/lena.jpg',
                        thumb: 'img/lena.jpg'
                    }
                ],
                subcomments: [
                ]
            }
        ],
        related: 1,
        peers: 6,
        conversations: 3
    },
    {
        name: 'Julia',
        question: 'Vegan diet in diabetes treatment?',
        published: new Date('19.04.2017'),
        rate: 10,
        images: [
            {
                full: 'img/lena.jpg',
                thumb: 'img/lena.jpg'
            }
        ],
        comments: 7,
        related: 2,
        peers: 7,
        conversations: 4
    },
    {
        name: 'Maria',
        question: 'Vegan diet to stop diabetes progress',
        published: new Date('18.04.2017'),
        rate: 11,
        images: [
            {
                full: 'img/lena.jpg',
                thumb: 'img/lena.jpg'
            }
        ],
        comments: 3,
        related: 3,
        peers: 2,
        conversations: 1
    },
    {
        name: 'Anna',
        question: 'Lorem ipsum dolor sit amet?',
        published: new Date('20.03.2017'),
        rate: 18,
        images: [
            {
                full: 'img/lena.jpg',
                thumb: 'img/lena.jpg'
            }
        ],
        comments: 5,
        related: 3,
        peers: 4,
        conversations: 1
    },
    {
        name: 'Eva',
        question: 'Will insulin make my patient gain weight?',
        published: new Date('20.04.2016'),
        rate: 22,
        images: [
            {
                full: 'img/lena.jpg',
                thumb: 'img/lena.jpg'
            }
        ],
        comments: 5,
        related: 1,
        peers: 6,
        conversations: 3
    },
    {
        name: 'Julia',
        question: 'Vegan diet in diabetes treatment?',
        published: new Date('19.04.2016'),
        rate: 10,
        images: [
            {
                full: 'img/lena.jpg',
                thumb: 'img/lena.jpg'
            }
        ],
        comments: 7,
        related: 2,
        peers: 7,
        conversations: 4
    },
    {
        name: 'Maria',
        question: 'Vegan diet to stop diabetes progress',
        published: new Date('18.04.2016'),
        rate: 0,
        images: [
            {
                full: 'img/lena.jpg',
                thumb: 'img/lena.jpg'
            }
        ],
        comments: 3,
        related: 3,
        peers: 2,
        conversations: 1
    },
    {
        name: 'Anna',
        question: 'Lorem ipsum dolor sit amet?',
        published: new Date('20.03.2016'),
        rate: 1,
        images: [
            {
                full: 'img/lena.jpg',
                thumb: 'img/lena.jpg'
            }
        ],
        comments: 5,
        related: 3,
        peers: 4,
        conversations: 1
    }

];