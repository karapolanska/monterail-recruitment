Dear Monterail Team,
hereby I deliver my completed recruitment tasks.


In the project the following technologies were used:

- Bootstrap

- jQuery

- Angular 1.4

Description:

1. Bootstrap - I use Bootstrap for almost all of my projects - I like the grid system and also some of the components such as ready-to-use modals or tabs are pretty handy. I got used to Bootstrap as my previous company demanded it in every project, however, if needed I can code every part on my own.

2. jQuery - I'm not sure if I can use pure JavaScript. Always using jQuery. In this project it is there only for Bootstrap as there was no need for my own scripts.

3. Angular - It's a new thing for me as in freelance job, concentrated more on visit cards rather than apps there has never been occasion to use it, but I'm astonished by its possibilities and eager to learn much more. Project is placed on BitBucket as I got free, unlimited account from the University and, as we used it during classes, I'm most familiar with this repository.



Completed tasks:
 
1 - Implement views

2 - Responsiveness

3 - User profile as modal

5 - Pagination and sorting

6 - Search

7 - Voting


Detailed description:

1, 2 - I hope you find my implementation satisfactory. I used different fonts,
    as the original one is not free of charge. I also replaced all images with
    font awesome, what would never happen if I had PSD file to take images from.
    The same goes with font sizes and dimensions - I always try to be as
    accurate as possible, but I couldn't take exact measures from PNG.
    
3 - Bootstrap was used for this purpose. Decided to leave it as a static component 
    without loading anything from angular.
    
4 (Templates) - I know very little about templates, however with angular it is 
    possible to load much of the content form js file instead of hardcodding
    them.
    
5, 6, 7 - done with Angular.

8 (routing) - I don't pretend to have any knowledge here but I will be happy to
    learn SPA while working in Monterail.



Thank you for your time taken by reviewing my tasks and thank you in advance
for your feedback, whatever the result will be.
After all it's all about getting better.